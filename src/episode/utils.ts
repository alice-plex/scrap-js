import $ from "cheerio";
import _ from "lodash";

async function baseFetch(url: string, options: RequestInit): Promise<Response> {
  const mergeOptions = {
    headers: {
      "User-Agent":
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36"
    },
    ...options
  };
  return fetch(url, mergeOptions);
}

async function fetchHtml(url: string, options: RequestInit): Promise<string> {
  return baseFetch(url, options).then(res => res.text());
}

export const fetchJson = _.memoize(
  async (url: string, options: RequestInit = {}): Promise<any> =>
    baseFetch(url, options).then(res => res.json())
);

async function fetchDomImpl(
  url: string,
  options: RequestInit = {},
  cheerioOptions?: CheerioOptionsInterface
): Promise<CheerioStatic> {
  const html = await fetchHtml(url, options);
  return $.load(html, cheerioOptions);
}

export const fetchDom = _.memoize(fetchDomImpl);

/* Only used for unit test */
export function clearCache(): void {
  fetchJson.cache = new Map();
  fetchDom.cache = new Map();
}
