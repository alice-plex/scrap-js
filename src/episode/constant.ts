import { Episode } from "@aliceplex/schema";

import {
  BaseEpisodeScraper,
  EpisodeFields,
  EpisodeScraperOptions
} from "./base";

export type EpisodeConstant = {
  [key: number]: Partial<Episode>;
};

export type ConstantEpisodeScraperOptions = {
  constant?: Partial<Episode>;
  episodeConstant?: EpisodeConstant;
  mergeDefault?: boolean;
};

export class ConstantEpisodeScraper extends BaseEpisodeScraper {
  private constant: Partial<Episode>;
  private episodeConstant: EpisodeConstant;
  private mergeDefault: boolean;

  public constructor(options: ConstantEpisodeScraperOptions = {}) {
    super();
    const {
      constant = {},
      episodeConstant = {},
      mergeDefault = false
    } = options;
    this.constant = constant;
    this.episodeConstant = episodeConstant;
    this.mergeDefault = mergeDefault;
  }

  public async scrap(
    episode: Partial<Episode>,
    options: EpisodeScraperOptions
  ): Promise<Partial<Episode>> {
    const newEpisode = { ...episode };
    const { episodeNumber } = options;
    const constant = this.getConstant(episodeNumber);
    Object.values(EpisodeFields).forEach(field => {
      if (field in constant) {
        newEpisode[field] = constant[field];
      }
    });
    return newEpisode;
  }

  private getConstant(episodeNumber: number): Partial<Episode> {
    const episodeConfig = this.episodeConstant[episodeNumber];
    if (episodeConfig) {
      return this.mergeDefault
        ? { ...this.constant, ...episodeConfig }
        : episodeConfig;
    }
    return { ...this.constant };
  }
}
