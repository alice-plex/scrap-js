import { Episode } from "@aliceplex/schema";

import { BaseEpisodeScraper, EpisodeScraperOptions } from "./base";

import { fetchDom } from "./utils";

export type HtmlEpisodeScraperOptions = {
  fetchOptions?: RequestInit;
  cheerioOptions?: CheerioOptionsInterface;
};

export abstract class HtmlEpisodeScraper extends BaseEpisodeScraper {
  private fetchOptions?: RequestInit;
  private cheerioOptions?: CheerioOptionsInterface;

  public constructor(options: HtmlEpisodeScraperOptions = {}) {
    super();
    const { fetchOptions, cheerioOptions } = options;
    this.fetchOptions = fetchOptions;
    this.cheerioOptions = cheerioOptions;
  }

  public async scrap(
    episode: Partial<Episode>,
    options: EpisodeScraperOptions
  ): Promise<Partial<Episode>> {
    const newEpisode = { ...episode };
    const url = this.getUrl(options);
    const dom = await fetchDom(url, this.fetchOptions, this.cheerioOptions);
    return this.parseHtml(newEpisode, options, dom);
  }

  public async thumbnail(
    options: EpisodeScraperOptions
  ): Promise<string | null> {
    const url = this.getUrl(options);
    const dom = await fetchDom(url, this.fetchOptions, this.cheerioOptions);
    return this.parseThumbnail(options, dom);
  }

  abstract getUrl(options: EpisodeScraperOptions): string;

  abstract parseHtml(
    episode: Partial<Episode>,
    options: EpisodeScraperOptions,
    dom: CheerioStatic
  ): Partial<Episode>;

  abstract parseThumbnail(
    options: EpisodeScraperOptions,
    dom: CheerioStatic
  ): Partial<string | null>;
}
