import { Episode, normalize } from "@aliceplex/schema";
import { parseTable } from "@joshuaavalon/cheerio-table-parser";
import $ from "cheerio";
import moment from "moment";

import { Omit, Overwrite } from "../type";

import { BaseEpisodeScraper, EpisodeScraperOptions } from "./base";
import { fetchDom } from "./utils";

export enum WikiEpisodeFields {
  Title = "title",
  Aired = "aired",
  Directors = "directors",
  Writers = "writers"
}

export type WikiEpisodeScraperOptions = {
  url: string;
  tableOffset?: number;
  mapping?: Map<WikiEpisodeFields, number>;
  multipleRow?: number;
};

export class WikiEpisodeScraper extends BaseEpisodeScraper {
  protected url: string;
  private tableOffset: number;
  private mapping: Map<WikiEpisodeFields, number>;
  private multipleRow: number | null;
  protected table?: string[][];

  public constructor(options: WikiEpisodeScraperOptions) {
    super();
    const {
      url,
      tableOffset = 0,
      mapping = new Map(),
      multipleRow = null
    } = options;
    this.url = url;
    this.tableOffset = tableOffset;
    this.mapping = mapping;
    this.multipleRow = multipleRow;
  }

  public async scrap(
    episode: Partial<Episode>,
    options: EpisodeScraperOptions
  ): Promise<Partial<Episode>> {
    const newEpisode = { ...episode };
    return this.multipleRow === null || this.multipleRow < 0
      ? this.singleScrap(newEpisode, options)
      : this.multiScrap(newEpisode, options);
  }

  protected parseSingleValue(field: WikiEpisodeFields, value: string): any {
    switch (field) {
      case WikiEpisodeFields.Title:
        return [normalize(value).replace(/\n+/g, " ")];
      case WikiEpisodeFields.Aired:
        return moment(value).format("YYYY-MM-DD");
      case WikiEpisodeFields.Directors:
      case WikiEpisodeFields.Writers:
        return value.split(/\s+/);
    }
  }

  protected getRowNumber(options: EpisodeScraperOptions): number {
    return options.episodeNumber;
  }

  protected async singleScrap(
    episode: Partial<Episode>,
    options: EpisodeScraperOptions
  ): Promise<Partial<Episode>> {
    const rowNum = this.getRowNumber(options);
    const table = await this.fetchTable();
    const row = table[rowNum];
    this.mapping.forEach((col, field) => {
      const value = this.parseSingleValue(field, row[col]);
      this.setValue(episode, field, value);
    });
    return episode;
  }

  protected setValue(
    episode: Partial<Episode>,
    field: WikiEpisodeFields,
    value: any
  ): void {
    switch (field) {
      case WikiEpisodeFields.Title:
        episode.title = value;
        break;
      case WikiEpisodeFields.Aired:
        episode.aired = value;
        break;
      case WikiEpisodeFields.Directors:
        episode.directors = value;
        break;
      case WikiEpisodeFields.Writers:
        episode.writers = value;
        break;
    }
  }

  private async mapRows(): Promise<{ [key: number]: number[] }> {
    const rowMap: { [key: number]: number[] } = {};
    let index = 0;
    let prev: string | null = null;
    const multipleRow = this.multipleRow as number;
    const table = await this.fetchTable();
    table.forEach((cols, row) => {
      if (!(index in rowMap)) {
        rowMap[index] = [];
      }
      const current = cols[multipleRow];
      if (current === prev || row === 0) {
        rowMap[index].push(row);
      } else {
        index++;
        rowMap[index] = [row];
      }
      prev = current;
    });
    return rowMap;
  }

  protected parseMultiValue(field: WikiEpisodeFields, values: string[]): any {
    switch (field) {
      case WikiEpisodeFields.Title:
        return values.map(v => normalize(v).replace(/\n+/g, " "));
      case WikiEpisodeFields.Aired:
        return moment(values[0]).format("YYYY-MM-DD");
      case WikiEpisodeFields.Directors:
      case WikiEpisodeFields.Writers: {
        const result: string[] = [];
        values.forEach(value => {
          value.split(/\s+/).forEach(v => {
            const nv = normalize(v);
            if (result.indexOf(nv) < 0) {
              result.push(nv);
            }
          });
        });
        return result;
      }
    }
  }

  protected async multiScrap(
    episode: Partial<Episode>,
    options: EpisodeScraperOptions
  ): Promise<Partial<Episode>> {
    const rowNum = this.getRowNumber(options);
    const rowMap = (await this.mapRows())[rowNum];
    const table = await this.fetchTable();
    this.mapping.forEach((col, field) => {
      const values = rowMap.map(row => table[row][col]);
      const value = this.parseMultiValue(field, values);
      this.setValue(episode, field, value);
    });
    return episode;
  }

  protected onParseTable(element: CheerioElement): string {
    $("sup", element).remove();
    $("style", element).remove();
    $("ruby", element).each((_, ruby) => {
      if ($("rp", ruby).length > 0) {
        return;
      }
      $("rt", ruby)
        .prepend("(")
        .append(")");
    });
    $("br", element).replaceWith("\n");
    $("hr", element).replaceWith("\n");
    return $(element).text();
  }

  protected async fetchTable(): Promise<string[][]> {
    if (this.table) {
      return this.table;
    }
    const dom = await fetchDom(this.url);
    const tables = dom("table.wikitable").toArray();
    if (tables.length <= this.tableOffset) {
      throw new Error(`There are only ${tables.length} table(s).`);
    }
    const table = parseTable(tables[this.tableOffset], {
      parser: this.onParseTable.bind(this)
    });
    this.table = table;
    return table;
  }
}

export type SmartWikiEpisodeScraperOptions = {
  tag: string;
  tagOffset?: number;
  expect?: Map<number[], string>;
} & Omit<WikiEpisodeScraperOptions, "tableOffset">;

export class SmartWikiEpisodeScraper extends WikiEpisodeScraper {
  private tag: string;
  private tagOffset: number;
  private expect: Map<number[], string>;

  public constructor(options: SmartWikiEpisodeScraperOptions) {
    super(options);
    const { tag, tagOffset = 0, expect = new Map() } = options;
    this.tag = tag;
    this.tagOffset = tagOffset;
    this.expect = expect;
  }

  protected async fetchTable(): Promise<string[][]> {
    if (this.table) {
      return this.table;
    }
    const dom = await fetchDom(this.url);
    const tables = dom(`#${this.tag}`)
      .nextAll("table")
      .toArray();
    if (tables.length <= this.tagOffset) {
      throw new Error(`There are only ${tables.length} table(s).`);
    }
    const table = parseTable(tables[this.tagOffset], {
      parser: this.onParseTable.bind(this)
    });
    this.table = table;
    this.expect.forEach((value, [row, col]) => {
      const actual = table[row][col];
      if (actual !== value) {
        throw new Error(`Expect ${value} at ${row}:${col}. Actual: ${actual}`);
      }
    });
    return table;
  }
}

export type JpSmartWikiEpisodeScraperOptions = Overwrite<
  Omit<SmartWikiEpisodeScraperOptions, "url">,
  {
    subject: string;
    tag?: string;
    tagOffset?: number;
  }
>;

export class JpSmartWikiEpisodeScraper extends SmartWikiEpisodeScraper {
  public constructor(options: JpSmartWikiEpisodeScraperOptions) {
    const { subject, tag = "各話リスト", tagOffset = 0 } = options;
    const url = `https://ja.wikipedia.org/wiki/${subject}`;
    super({ ...options, url, tag, tagOffset });
  }
}
