import { Episode } from "@aliceplex/schema";

export type EpisodeScraperOptions = {
  seasonNumber: number;
  episodeNumber: number;
};

export interface EpisodeScraper {
  scrap(
    episode: Partial<Episode>,
    options: EpisodeScraperOptions
  ): Promise<Partial<Episode>>;

  thumbnail(options: EpisodeScraperOptions): Promise<string | null>;
}

export abstract class BaseEpisodeScraper implements EpisodeScraper {
  abstract async scrap(
    episode: Partial<Episode>,
    options: EpisodeScraperOptions
  ): Promise<Partial<Episode>>;

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  public async thumbnail(_: EpisodeScraperOptions): Promise<string | null> {
    return null;
  }
}

export class CombineEpisodeScraper implements EpisodeScraper {
  protected scrapers: EpisodeScraper[];

  public constructor(scrapers: EpisodeScraper[]) {
    this.scrapers = scrapers;
  }

  public async scrap(
    episode: Partial<Episode>,
    options: EpisodeScraperOptions
  ): Promise<Partial<Episode>> {
    const promises = this.scrapers.map(scraper =>
      scraper.scrap(episode, options)
    );
    return Promise.all(promises).then(episodes =>
      episodes.reduce((previous, current) => ({ ...previous, ...current }), {
        ...episode
      })
    );
  }

  public async thumbnail(
    options: EpisodeScraperOptions
  ): Promise<string | null> {
    for (let scraper of this.scrapers) {
      const thumbnail = scraper.thumbnail(options);
      if (thumbnail) {
        return thumbnail;
      }
    }
    return null;
  }
}

export enum EpisodeFields {
  Title = "title",
  ContentRating = "content_rating",
  Aired = "aired",
  Summary = "summary",
  Rating = "rating",
  Directors = "directors",
  Writers = "writers"
}
