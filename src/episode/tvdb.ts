import { Episode } from "@aliceplex/schema";
import { URL } from "url";
import moment from "moment";

import { BaseEpisodeScraper, EpisodeScraperOptions } from "./base";

import { fetchJson } from "./utils";

export type TvdbCredential = {
  apikey: string;
  userkey: string;
  username: string;
};

export enum TvdbEpisodeFields {
  Title = "title",
  Aired = "aired",
  Summary = "summary",
  Thumbnail = "thumbnail"
}

export type TvdbEpisodeData = {
  absoluteNumber: number;
  airedEpisodeNumber: number;
  airedSeason: number;
  airsAfterSeason: number;
  airsBeforeEpisode: number;
  airsBeforeSeason: number;
  director: string;
  directors: string[];
  dvdChapter: number;
  dvdDiscid: string;
  dvdEpisodeNumber: number;
  dvdSeason: number;
  episodeName: string;
  filename: string;
  firstAired: string;
  guestStars: string[];
  id: number;
  imdbId: string;
  lastUpdated: number;
  lastUpdatedBy: string;
  overview: string;
  productionCode: string;
  seriesId: string;
  showUrl: string;
  siteRating: number;
  siteRatingCount: number;
  thumbAdded: string;
  thumbAuthor: number;
  thumbHeight: string;
  thumbWidth: string;
  writers: string[];
};

type TvdbLinks = {
  first: number | null;
  last: number | null;
  next: number | null;
  previous: number | null;
};

type TvdbSeriesEpisodes = {
  data: TvdbEpisodeData[];
  links: TvdbLinks;
};

export type TvdbEpisodeScraperOptions = {
  tvdbId: string;
  credential: TvdbCredential;
  fields?: TvdbEpisodeFields[];
  language?: string;
};

export class TvdbEpisodeScraper extends BaseEpisodeScraper {
  private credential: TvdbCredential;
  private fields: TvdbEpisodeFields[];
  private language: string;
  private tvdbId: string;
  private authToken: string;
  private episodes?: TvdbEpisodeData[];

  public constructor(options: TvdbEpisodeScraperOptions) {
    super();
    const { tvdbId, credential, fields = [], language = "ja" } = options;
    this.credential = credential;
    this.fields = fields;
    this.language = language;
    this.tvdbId = tvdbId;
    this.authToken = "";
  }

  public async scrap(
    episode: Partial<Episode>,
    options: EpisodeScraperOptions
  ): Promise<Partial<Episode>> {
    const newEpisode = { ...episode };
    const data = await this.fetchEpisode(options);
    if (this.fields.indexOf(TvdbEpisodeFields.Title) >= 0) {
      newEpisode.title = [data.episodeName];
    }
    if (this.fields.indexOf(TvdbEpisodeFields.Summary) >= 0) {
      newEpisode.summary = data.overview;
    }
    if (this.fields.indexOf(TvdbEpisodeFields.Aired) >= 0) {
      const date = moment(data.firstAired);
      newEpisode.aired = date.format("YYYY-MM-DD");
    }
    return newEpisode;
  }

  public async thumbnail(
    options: EpisodeScraperOptions
  ): Promise<string | null> {
    if (this.fields.indexOf(TvdbEpisodeFields.Thumbnail) < 0) {
      return null;
    }
    const data = await this.fetchEpisode(options);
    const thumbnail = data.filename;
    return thumbnail ? `https://www.thetvdb.com/banners/${thumbnail}` : null;
  }

  protected async fetchAuthToken(): Promise<void> {
    if (this.authToken) {
      return;
    }
    const url = "https://api.thetvdb.com/login";
    const json = await fetchJson(url, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(this.credential)
    });
    this.authToken = json.token;
  }

  protected getAuthHeaders(): any {
    return {
      Accept: "application/json",
      Authorization: `Bearer ${this.authToken}`,
      "Accept-Language": this.language
    };
  }

  protected async fetchEpisodes(
    pageNumber: number = 1
  ): Promise<TvdbEpisodeData[]> {
    const url = new URL(
      `https://api.thetvdb.com/series/${this.tvdbId}/episodes`
    );
    const page = "" + pageNumber;
    url.searchParams.append("page", page);
    const headers = this.getAuthHeaders();
    const json: TvdbSeriesEpisodes = await fetchJson(url.toString(), {
      headers
    });
    const data = json.data;
    const next = json.links.next;
    if (next) {
      const newData = await this.fetchEpisodes(next);
      data.push(...newData);
    }
    return data;
  }

  protected async fetchEpisode(
    options: EpisodeScraperOptions
  ): Promise<TvdbEpisodeData> {
    await this.fetchAuthToken();
    const seasonNumber = this.getSeasonNumber(options);
    const episodeNumber = this.getEpisodeNumber(options);
    let episodes: TvdbEpisodeData[];
    if (!this.episodes) {
      episodes = await this.fetchEpisodes();
      this.episodes = episodes;
    } else {
      episodes = this.episodes;
    }
    for (const episode of episodes) {
      const { airedSeason, airedEpisodeNumber } = episode;
      if (
        airedSeason === seasonNumber ||
        airedEpisodeNumber === episodeNumber
      ) {
        return episode;
      }
    }
    throw new Error(
      `Cannot find season ${seasonNumber} episode ${episodeNumber}`
    );
  }

  protected getSeasonNumber(options: EpisodeScraperOptions): number {
    return options.seasonNumber;
  }

  protected getEpisodeNumber(options: EpisodeScraperOptions): number {
    return options.episodeNumber;
  }
}
