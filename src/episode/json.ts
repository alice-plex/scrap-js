import { Episode } from "@aliceplex/schema";

import { BaseEpisodeScraper, EpisodeScraperOptions } from "./base";

import { fetchJson } from "./utils";

export type JsonEpisodeScraperOptions = {
  fetchOptions?: RequestInit;
};

export abstract class JsonEpisodeScraper extends BaseEpisodeScraper {
  private fetchOptions?: RequestInit;

  public constructor(options: JsonEpisodeScraperOptions = {}) {
    super();
    const { fetchOptions } = options;
    this.fetchOptions = fetchOptions;
  }

  public async scrap(
    episode: Partial<Episode>,
    options: EpisodeScraperOptions
  ): Promise<Partial<Episode>> {
    const newEpisode = { ...episode };
    const url = this.getUrl(options);
    const json = await fetchJson(url, this.fetchOptions);
    return this.parseJson(newEpisode, options, json);
  }

  public async thumbnail(
    options: EpisodeScraperOptions
  ): Promise<string | null> {
    const url = this.getUrl(options);
    const json = await fetchJson(url, this.fetchOptions);
    return this.parseThumbnail(options, json);
  }

  abstract getUrl(options: EpisodeScraperOptions): string;

  abstract parseJson(
    episode: Partial<Episode>,
    options: EpisodeScraperOptions,
    json: any
  ): Partial<Episode>;

  abstract parseThumbnail(
    options: EpisodeScraperOptions,
    json: any
  ): Partial<string | null>;
}
