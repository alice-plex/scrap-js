# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.1.0]

### Added

- Add `CombineEpisodeScraper`.

## 1.0.0

### Added

- Initial Release.

[unreleased]: https://gitlab.com/alice-plex/schema-js/compare/1.1.0...master
[1.1.0]: https://gitlab.com/alice-plex/schema-js/compare/1.0.0...1.1.0
