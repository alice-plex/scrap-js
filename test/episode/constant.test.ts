import { ConstantEpisodeScraper } from "../../src/episode";

test("Test ConstantEpisodeScraper constant", async () => {
  const title = ["title"];
  const scraper = new ConstantEpisodeScraper({
    constant: { title }
  });
  const episode = await scraper.scrap(
    {},
    { episodeNumber: 1, seasonNumber: 1 }
  );
  expect(episode.title).toEqual(title);
});

test("Test ConstantEpisodeScraper episode constant", async () => {
  const title = ["title"];
  const scraper = new ConstantEpisodeScraper({
    episodeConstant: {
      [1]: { title }
    }
  });
  const episode = await scraper.scrap(
    {},
    { episodeNumber: 1, seasonNumber: 1 }
  );
  expect(episode.title).toEqual(title);
});

test("Test ConstantEpisodeScraper merge constants", async () => {
  const title = ["title"];
  const aired = "2019-01-01";
  const rating = 5;
  const scraper = new ConstantEpisodeScraper({
    constant: { aired, rating: 10 },
    episodeConstant: {
      [1]: { title, rating }
    },
    mergeDefault: true
  });
  const episode = await scraper.scrap(
    {},
    { episodeNumber: 1, seasonNumber: 1 }
  );
  expect(episode.title).toEqual(title);
  expect(episode.aired).toEqual(aired);
  expect(episode.rating).toEqual(rating);
});

test("Test ConstantEpisodeScraper default options", async () => {
  const scraper = new ConstantEpisodeScraper();
  const episode = await scraper.scrap(
    {},
    { episodeNumber: 1, seasonNumber: 1 }
  );
  expect(episode).toEqual({});
});
