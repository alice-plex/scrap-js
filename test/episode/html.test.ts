import { Episode } from "@aliceplex/schema";
import { mockFetch, mockResponseOnce, resetMocks } from "jest-fetch";

import { EpisodeScraperOptions, HtmlEpisodeScraper } from "../../src/episode";
import { clearCache } from "../../src/episode/utils";

class TestHtmlEpisodeScraper extends HtmlEpisodeScraper {
  public getUrl(): string {
    return "https://example.com";
  }

  public parseHtml(
    episode: Partial<Episode>,
    _: EpisodeScraperOptions,
    dom: CheerioStatic
  ): Partial<Episode> {
    episode.title = [dom("#test").text()];
    return episode;
  }

  public parseThumbnail(): string | null {
    return "https://example.co/thumbnail";
  }
}

beforeEach(() => {
  resetMocks();
  clearCache();
});

test("Test HtmlEpisodeScraper html", async () => {
  mockResponseOnce(
    '<html><head></head><body><p id="test">title</p></body></html>'
  );

  const scraper = new TestHtmlEpisodeScraper();
  const episode = await scraper.scrap(
    {},
    {
      episodeNumber: 1,
      seasonNumber: 1
    }
  );

  expect(episode.title).toEqual(["title"]);
  expect(mockFetch.mock.calls.length).toEqual(1);
  expect(mockFetch.mock.calls[0][0]).toEqual("https://example.com");
});

test("Test HtmlEpisodeScraper thumbnail", async () => {
  mockResponseOnce(
    '<html><head></head><body><p id="test">title</p></body></html>'
  );

  const scraper = new TestHtmlEpisodeScraper();
  const thumbnail = await scraper.thumbnail({
    episodeNumber: 1,
    seasonNumber: 1
  });

  expect(thumbnail).toEqual("https://example.co/thumbnail");
  expect(mockFetch.mock.calls.length).toEqual(1);
  expect(mockFetch.mock.calls[0][0]).toEqual("https://example.com");
});
