import { mockResponse, resetMocks } from "jest-fetch";
import { BodyInit, Request, RequestInit } from "node-fetch";

import { TvdbEpisodeFields, TvdbEpisodeScraper } from "../../src/episode";
import { clearCache } from "../../src/episode/utils";

beforeEach(() => {
  resetMocks();
  clearCache();
});

async function mockTvdbApi(
  url: string | Request,
  init?: RequestInit
): Promise<BodyInit> {
  switch (url) {
    case "https://api.thetvdb.com/login":
      return JSON.stringify({
        token: "token"
      });
    case "https://api.thetvdb.com/series/123/episodes":
    case "https://api.thetvdb.com/series/123/episodes?page=1": {
      // eslint-disable-next-line @typescript-eslint/no-object-literal-type-assertion
      const { headers = {} } = init || ({} as RequestInit);
      const { Authorization } = headers as any;
      if (Authorization !== "Bearer token") {
        return "";
      }
      return JSON.stringify({
        links: {
          first: 1,
          last: 1,
          next: null,
          prev: null
        },
        data: [
          {
            id: 127131,
            airedSeason: 1,
            airedSeasonID: 6345,
            airedEpisodeNumber: 1,
            episodeName: "Pilot (1)",
            firstAired: "2004-09-22",
            guestStars: ["Greg Grunberg", "John Dixon", " Michelle Arthur"],
            director: "J.J. Abrams",
            directors: ["J.J. Abrams"],
            writers: ["J.J. Abrams", "Damon Lindelof"],
            overview: "overview",
            language: {
              episodeName: "en",
              overview: "en"
            },
            productionCode: "100",
            showUrl: "http://www.tv.com/episode/334467/summary.html",
            lastUpdated: 1484282112,
            dvdDiscid: "",
            dvdSeason: 1,
            dvdEpisodeNumber: 1,
            dvdChapter: null,
            absoluteNumber: null,
            filename: "episodes/73739/127131.jpg",
            seriesId: 73739,
            lastUpdatedBy: 471903,
            airsAfterSeason: null,
            airsBeforeSeason: null,
            airsBeforeEpisode: null,
            thumbAuthor: 2011,
            thumbAdded: "",
            thumbWidth: "400",
            thumbHeight: "225",
            imdbId: "tt0636289",
            siteRating: 8,
            siteRatingCount: 129
          }
        ]
      });
    }
    default:
      console.log(url);
      return "";
  }
}

test("Test TvdbEpisodeScraper episode", async () => {
  mockResponse(mockTvdbApi);

  const scraper = new TvdbEpisodeScraper({
    tvdbId: "123",
    credential: {
      apikey: "apikey",
      userkey: "userkey",
      username: "username"
    },
    fields: [
      TvdbEpisodeFields.Title,
      TvdbEpisodeFields.Aired,
      TvdbEpisodeFields.Summary
    ]
  });
  const episode = await scraper.scrap(
    {},
    { episodeNumber: 1, seasonNumber: 1 }
  );

  expect(episode.title).toEqual(["Pilot (1)"]);
  expect(episode.summary).toEqual("overview");
  expect(episode.aired).toEqual("2004-09-22");
});

test("Test TvdbEpisodeScraper thumbnail", async () => {
  mockResponse(mockTvdbApi);

  const scraper = new TvdbEpisodeScraper({
    tvdbId: "123",
    credential: {
      apikey: "apikey",
      userkey: "userkey",
      username: "username"
    },
    fields: [TvdbEpisodeFields.Thumbnail]
  });

  const thumbnail = await scraper.thumbnail({
    episodeNumber: 1,
    seasonNumber: 1
  });
  expect(thumbnail).toEqual(
    "https://www.thetvdb.com/banners/episodes/73739/127131.jpg"
  );
});
