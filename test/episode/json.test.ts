import { Episode } from "@aliceplex/schema";
import { mockFetch, mockResponseOnce, resetMocks } from "jest-fetch";

import { EpisodeScraperOptions, JsonEpisodeScraper } from "../../src/episode";
import { clearCache } from "../../src/episode/utils";

class TestJsonEpisodeScraper extends JsonEpisodeScraper {
  public getUrl(): string {
    return "https://example.com";
  }

  public parseJson(
    episode: Partial<Episode>,
    _: EpisodeScraperOptions,
    json: any
  ): Partial<Episode> {
    episode.title = json.title;
    return episode;
  }

  public parseThumbnail(
    options: EpisodeScraperOptions,
    json: any
  ): string | null {
    return json.thumbnail[options.episodeNumber];
  }
}

beforeEach(() => {
  resetMocks();
  clearCache();
});

test("Test JsonEpisodeScraper json", async () => {
  const title = ["title"];
  mockResponseOnce(JSON.stringify({ title }));

  const scraper = new TestJsonEpisodeScraper();
  const episode = await scraper.scrap(
    {},
    { episodeNumber: 1, seasonNumber: 1 }
  );

  expect(episode.title).toEqual(title);
  expect(mockFetch.mock.calls.length).toEqual(1);
  expect(mockFetch.mock.calls[0][0]).toEqual("https://example.com");
});

test("Test JsonEpisodeScraper thumbnail", async () => {
  const url = "https://example.co/thumbnail";
  mockResponseOnce(
    JSON.stringify({
      thumbnail: {
        [1]: url
      }
    })
  );

  const scraper = new TestJsonEpisodeScraper();
  const thumbnail = await scraper.thumbnail({
    episodeNumber: 1,
    seasonNumber: 1
  });

  expect(thumbnail).toEqual(url);
  expect(mockFetch.mock.calls.length).toEqual(1);
  expect(mockFetch.mock.calls[0][0]).toEqual("https://example.com");
});
