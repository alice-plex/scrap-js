import { mockFetch, mockResponseOnce, resetMocks } from "jest-fetch";

import {
  SmartWikiEpisodeScraper,
  WikiEpisodeFields,
  WikiEpisodeScraper
} from "../../src/episode";
import { clearCache } from "../../src/episode/utils";

beforeEach(() => {
  resetMocks();
  clearCache();
});

test("Test WikiEpisodeScraper", async () => {
  mockResponseOnce(`<table class="wikitable">
  <tr>
   <th>Name</th>
   <th>Favorite Color</th>
  </tr>
  <tr>
   <td>Bob</td>
   <td>Yellow</td>
  </tr>
  <tr>
   <td>Michelle</td>
   <td>Purple</td>
  </tr>
 </table>`);

  const scraper = new WikiEpisodeScraper({
    url: "https://example.com",
    mapping: new Map([[WikiEpisodeFields.Title, 1]])
  });
  const episode = await scraper.scrap(
    {},
    { episodeNumber: 1, seasonNumber: 1 }
  );

  expect(episode.title).toEqual(["Yellow"]);
  expect(mockFetch.mock.calls.length).toEqual(1);
  expect(mockFetch.mock.calls[0][0]).toEqual("https://example.com");
});

test("Test WikiEpisodeScraper multiple row", async () => {
  mockResponseOnce(`<table class="wikitable">
  <tr>
    <th>Number</th>
    <th>Name</th>
    <th>Favorite Color</th>
  </tr>
  <tr>
    <td rowspan="2">1</td>
    <td>Bob</td>
    <td>Red</td>
  </tr>
  <tr>
    <td colspan="4">Alice</td>
  </tr>
  <tr>
    <td>2</td>
    <td>Michelle</td>
    <td>Purple</td>
  </tr>
 </table>`);

  const scraper = new WikiEpisodeScraper({
    url: "https://example.com",
    multipleRow: 0,
    mapping: new Map([[WikiEpisodeFields.Title, 2]])
  });
  const episode = await scraper.scrap(
    {},
    { episodeNumber: 1, seasonNumber: 1 }
  );

  expect(episode.title).toEqual(["Red", "Alice"]);
  expect(mockFetch.mock.calls.length).toEqual(1);
  expect(mockFetch.mock.calls[0][0]).toEqual("https://example.com");
});

test("Test SmartWikiEpisodeScraper", async () => {
  mockResponseOnce(`<h1 id="title">Title</h1>
  <table class="wikitable">
  <tr>
   <th>Name</th>
   <th>Favorite Color</th>
  </tr>
  <tr>
   <td>Bob</td>
   <td>Yellow</td>
  </tr>
  <tr>
   <td>Michelle</td>
   <td>Purple</td>
  </tr>
 </table>
 <table class="wikitable">
 <tr>
  <th>Name</th>
  <th>Favorite Color</th>
 </tr>
 <tr>
  <td>Bob2</td>
  <td>Yellow2</td>
 </tr>
 <tr>
  <td>Michelle2</td>
  <td>Purple2</td>
 </tr>
</table>`);

  const scraper = new SmartWikiEpisodeScraper({
    url: "https://example.com",
    tag: "title",
    tagOffset: 1,
    mapping: new Map([[WikiEpisodeFields.Title, 1]])
  });
  const episode = await scraper.scrap(
    {},
    { episodeNumber: 1, seasonNumber: 1 }
  );

  expect(episode.title).toEqual(["Yellow2"]);
  expect(mockFetch.mock.calls.length).toEqual(1);
  expect(mockFetch.mock.calls[0][0]).toEqual("https://example.com");
});

test("Test SmartWikiEpisodeScraper expect", async () => {
  mockResponseOnce(`<h1 id="title">Title</h1>
  <table class="wikitable">
  <tr>
   <th>Name</th>
   <th>Favorite Color</th>
  </tr>
  <tr>
   <td>Bob</td>
   <td>Yellow</td>
  </tr>
  <tr>
   <td>Michelle</td>
   <td>Purple</td>
  </tr>
 </table>`);

  const scraper = new SmartWikiEpisodeScraper({
    url: "https://example.com",
    tag: "title",
    expect: new Map([[[0, 0], "Name"]]),
    mapping: new Map([[WikiEpisodeFields.Title, 1]])
  });
  const episode = await scraper.scrap(
    {},
    { episodeNumber: 1, seasonNumber: 1 }
  );

  expect(episode.title).toEqual(["Yellow"]);
  expect(mockFetch.mock.calls.length).toEqual(1);
  expect(mockFetch.mock.calls[0][0]).toEqual("https://example.com");
});

test("Test SmartWikiEpisodeScraper expect throw", async () => {
  mockResponseOnce(`<h1 id="title">Title</h1>
  <table class="wikitable">
  <tr>
   <th>Name</th>
   <th>Favorite Color</th>
  </tr>
  <tr>
   <td>Bob</td>
   <td>Yellow</td>
  </tr>
  <tr>
   <td>Michelle</td>
   <td>Purple</td>
  </tr>
 </table>`);

  const scraper = new SmartWikiEpisodeScraper({
    url: "https://example.com",
    tag: "title",
    expect: new Map([[[0, 0], "Bob"]]),
    mapping: new Map([[WikiEpisodeFields.Title, 1]])
  });

  await expect(
    scraper.scrap({}, { episodeNumber: 1, seasonNumber: 1 })
  ).rejects.toBeInstanceOf(Error);
  expect(mockFetch.mock.calls.length).toEqual(1);
  expect(mockFetch.mock.calls[0][0]).toEqual("https://example.com");
});
